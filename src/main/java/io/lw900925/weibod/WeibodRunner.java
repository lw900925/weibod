package io.lw900925.weibod;

import com.drew.imaging.jpeg.JpegMetadataReader;
import com.drew.imaging.jpeg.JpegProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.gson.*;
import io.lw900925.weibod.config.WeibodProperties;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class WeibodRunner implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(WeibodRunner.class);
    private static final Map<String, CacheInfo> USER_CACHE = new TreeMap<>();
    private static final List<String> DOWNLOAD_LIST = new ArrayList<>();
    private static final String USER = "user";
    private static final String TOP_ITEM = "top_item";
    private static final String TIMELINES = "timelines";
    private static final String LIVE_PHOTOS = "live_photos";
    private static final String LIVE_PICTURE = "pic";
    private static final String LIVE_MOVIE = "mov";
    private static final String ONLY_LIVE = "only_live";

    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Autowired
    private OkHttpClient okHttpClient;
    @Autowired
    private WeibodProperties properties;

    @PostConstruct
    private void postConstruct() throws IOException {
        // 从文件加载上次抓取最新微博ID
        Path cachePath = Paths.get(properties.getWeibo().getCacheDir());
        if (Files.exists(cachePath)) {
            try (InputStream inputStream = Files.newInputStream(cachePath)) {
                String jsonStr = StreamUtils.copyToString(inputStream, StandardCharsets.UTF_8);
                JsonArray cacheInfos = JsonParser.parseString(jsonStr).getAsJsonArray();
                cacheInfos.forEach(infoElement -> {
                    JsonObject info = infoElement.getAsJsonObject();
                    USER_CACHE.put(info.get("uid").getAsString(), new CacheInfo(info.get("screen_name").getAsString(), info.get("item_id").getAsString()));
                });
            }
        }

        // 初始化下载列表
        Path userPath = Paths.get(properties.getWeibo().getUserDir());
        if (Files.notExists(userPath)) {
            throw new NullPointerException("用户下载列表不存在");
        }
        Files.readAllLines(userPath).stream()
                .map(line -> line.substring(0, line.indexOf("[")))
                .filter(line -> !line.startsWith("#"))
                .forEach(DOWNLOAD_LIST::add);
    }

    @PreDestroy
    private void preDestroy() throws IOException {
        // 保存缓存文件
        List<Map<String, String>> map = USER_CACHE.entrySet().stream().map(entry -> ImmutableMap.<String, String> builder()
                .put("uid", entry.getKey())
                .put("screen_name", entry.getValue().getScreenName())
                .put("item_id", entry.getValue().getItemId())
                .build()).collect(Collectors.toList());

        String jsonStr = gson.toJson(map);
        Path path = Paths.get(properties.getWeibo().getCacheDir());
        Files.delete(path);
        Files.write(path, jsonStr.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE_NEW);
    }

    @Override
    public void run(String... args) throws Exception {
        DOWNLOAD_LIST.forEach(uid -> {
            // step1.获取用户微博
            Map<String, Object> map = getTimelines(uid);

            // step2.提取live_photo链接
            map = extract(map);

            // step3.下载
            download(map);
        });
    }

    public Map<String, Object> getTimelines(String uid) {
        boolean onlyLive = !uid.contains("-A");
        if (!onlyLive) {
            uid = StringUtils.replace(uid, "-A", "");
        }

        JsonObject userInfo = getUserInfo(uid);

        String screenName = userInfo.get("screen_name").getAsString();

        // statuses_count
        int count = userInfo.get("statuses_count").getAsInt();
        if (count == 0) {
            return null;
        }

        logger.debug("{} - 总共有{}条微博", screenName, count);

        // 一个简单的分页逻辑
        int page = 0;
        int size = properties.getWeibo().getSize();
        if (count % size == 0) {
            page = count / size;
        } else {
            page = (count / size) + 1;
        }

        String url = properties.getWeibo().getApi().getBaseUrl() + "/container/getIndex";
        Map<String, String> parameters = new HashMap<>();
        parameters.put("count", String.valueOf(size));
        parameters.put("containerid", "107603" + uid);

        JsonArray timelines = new JsonArray();
        for (int i=0; i<page; i++) {
            parameters.put("page", String.valueOf(i + 1));

            String jsonStr = httpGet(url, parameters);
            JsonObject jsonObject = JsonParser.parseString(jsonStr).getAsJsonObject();
            if (jsonObject.get("ok").getAsInt() == 0) {
                break;
            }

            JsonArray pageTimelines = jsonObject.get("data").getAsJsonObject().get("cards").getAsJsonArray();
            // 从缓存中获取最大记录
            CacheInfo cacheInfo = USER_CACHE.get(uid);
            if (cacheInfo != null) {
                long search = StreamSupport.stream(Spliterators.spliteratorUnknownSize(pageTimelines.iterator(), 0), false)
                        .map(JsonElement::getAsJsonObject)
                        .filter(timeline -> timeline.get("itemid").getAsString().equals(cacheInfo.getItemId()))
                        .count();
                if (search > 0) {
                    break;
                }
            }

            timelines.addAll(pageTimelines);
            logger.debug("{} - 第{}次抓取，本次返回{}条timeline", screenName, i + 1, pageTimelines.size());
        }

        logger.debug("{} - 所有timeline已经获取完毕，结果集中共包含{}条", screenName, timelines.size());

        Map<String, Object> map = new HashMap<>();
        if (timelines.size() > 0) {
            map.put(TOP_ITEM, timelines.get(0));
        }
        map.put(USER, userInfo);
        map.put(TIMELINES, timelines);
        map.put(ONLY_LIVE, onlyLive);
        return map;
    }

    private JsonObject getUserInfo(String uid) {
        String url = properties.getWeibo().getApi().getBaseUrl() + "/container/getIndex";
        String jsonStr = httpGet(url, ImmutableMap.<String, String> builder()
                .put("type", "uid")
                .put("value", uid)
                .build());

        JsonObject msg = JsonParser.parseString(jsonStr).getAsJsonObject();
        if (msg.get("ok").getAsInt() != 1) {
            logger.error("获取用户失败: {}", decode(jsonStr));
            throw new RuntimeException();
        }

        return msg.get("data").getAsJsonObject().get("userInfo").getAsJsonObject();
    }

    public String httpGet(String url, Map<String, String> parameters) {
        String jsonStr = null;

        String[] keyValuePairs = parameters.entrySet().stream().map(entry -> entry.getKey() + "=" + entry.getValue()).toArray(String[]::new);
        String strQueryParam = String.join("&", keyValuePairs);
        url = url + "?" + strQueryParam;
        Request request = new Request.Builder().url(url).get().build();
        try {
            Response response = okHttpClient.newCall(request).execute();
            if (response.isSuccessful() && response.body() != null) {
                jsonStr = response.body().string();
            } else {
                logger.debug("GET响应失败\nURL: {}\nstatus_code:{}\nmessage:{}", url, response.code(), response.message());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return jsonStr;
    }

    public static String decode(String str) {
        Pattern pattern = Pattern.compile("(\\\\u(\\p{XDigit}{4}))");
        Matcher matcher = pattern.matcher(str);
        char ch;
        while (matcher.find()) {
            ch = (char) Integer.parseInt(matcher.group(2), 16);
            str = str.replace(matcher.group(1), String.valueOf(ch));
        }
        return str;
    }

    // step2.抽取

    public Map<String, Object> extract(Map<String, Object> map) {
        List<Map<String, String>> list = new ArrayList<>();

        if (map == null) {
            return null;
        }

        JsonObject user = (JsonObject) map.get(USER);
        JsonArray timelines = (JsonArray) map.get(TIMELINES);
        boolean onlyLive = (boolean) map.get(ONLY_LIVE);

        if (timelines.size() == 0) {
            map.put(LIVE_PHOTOS, list);
            return map;
        }

        timelines.forEach(timeline -> {
            JsonObject mblog = timeline.getAsJsonObject().get("mblog").getAsJsonObject();

            // 判断是否有live_photo标签
            if (mblog.has("live_photo")) {
                JsonArray movies = mblog.get("live_photo").getAsJsonArray();
                JsonArray pictures = mblog.get("pics").getAsJsonArray();

                // LivePhoto视频与图片对应关系
                String picVideo = mblog.get("pic_video").getAsString();
                Arrays.stream(StringUtils.tokenizeToStringArray(picVideo, "\\,"))
                        .map(strVideoIds -> StringUtils.tokenizeToStringArray(strVideoIds, "\\:"))
                        .forEach(videoIds -> {
                            String picIndex = videoIds[0];
                            String videoId = videoIds[1];

                            String movUrl = StreamSupport.stream(Spliterators.spliteratorUnknownSize(movies.iterator(), Spliterator.ORDERED), false)
                                    .map(JsonElement::getAsString)
                                    .filter(movie -> movie.contains(videoId))
                                    .findAny().orElseThrow(() -> new RuntimeException("未找到对应的视频"));
                            String picUrl = pictures.get(Integer.parseInt(picIndex)).getAsJsonObject().get("large").getAsJsonObject().get("url").getAsString();

                            list.add(ImmutableMap.<String, String> builder()
                                    .put(LIVE_PICTURE, picUrl)
                                    .put(LIVE_MOVIE, movUrl)
                                    .build());
                        });
            }

            if (!onlyLive && !mblog.has("live_photo") && !mblog.get("pic_num").getAsString().equals("0")) {
                JsonArray pictures = mblog.get("pics").getAsJsonArray();
                StreamSupport.stream(Spliterators.spliteratorUnknownSize(pictures.iterator(), Spliterator.ORDERED), false)
                        .map(jsonElement -> jsonElement.getAsJsonObject().get("large").getAsJsonObject().get("url").getAsString())
                        .forEach(url -> list.add(ImmutableMap.of(LIVE_PICTURE, url)));
            }
        });

        map.put(LIVE_PHOTOS, list);

        logger.debug("{} - 链接已提取完毕，总共{}个", user.get("screen_name").getAsString(), list.size());
        return map;
    }

    @SuppressWarnings("unchecked")
    private void download(Map<String, Object> map) {
        if (map == null || ((JsonArray) map.get(TIMELINES)).size() == 0) {
            return;
        }

        JsonObject user = (JsonObject) map.get(USER);
        JsonObject topItem = (JsonObject) map.get(TOP_ITEM);
        List<Map<String, String>> livePhotos = (List<Map<String, String>>) map.get(LIVE_PHOTOS);

        String screenName = user.get("screen_name").getAsString();
        String destDir = properties.getWeibo().getDestDir();

        logger.debug("开始下载...");

        AtomicInteger indexAI = new AtomicInteger(0);
        AtomicLong filenameAL = new AtomicLong(System.currentTimeMillis());
        long count = livePhotos.stream().map(Map::entrySet).mapToLong(Collection::size).sum();

        livePhotos.stream().parallel().forEach(livePhoto -> {
            String picUrl = livePhoto.get(LIVE_PICTURE);
            String movUrl = livePhoto.get(LIVE_MOVIE);

            long timeMillis = filenameAL.incrementAndGet();
            Lists.newArrayList(picUrl, movUrl).stream().filter(Objects::nonNull).forEach(url -> {
                String filename = timeMillis + "." + getFileExtension(url);
                Request request = new Request.Builder().url(url).get().build();
                try (Response response = okHttpClient.newCall(request).execute()) {
                    if (response.isSuccessful()) {
                        try (InputStream inputStream = Objects.requireNonNull(response.body()).byteStream()) {
                            Path path = Paths.get(destDir, screenName, filename);
                            Files.createDirectories(path.getParent());
                            Files.copy(inputStream, path);

                            logger.debug("{} - 第[{}/{}]个文件下载完成：{}", screenName, indexAI.incrementAndGet(), count, path);
                        }
                    } else {
                        logger.error("code: {}, message: {}, url: {}", response.code(), response.message(), url);
                    }
                } catch (IOException e) {
                    logger.error("文件下载出错 - url: {}, filename: {}", url, filename);
                    logger.error(e.getMessage(), e);
                }
            });
        });

        // 文件重命名
        rename(Paths.get(destDir, screenName));

        // 所有媒体下载完成，更新timeline_id
        USER_CACHE.put(user.get("id").getAsString(), new CacheInfo(screenName, topItem.get("itemid").getAsString()));
    }

    private void rename(Path path) {
        AtomicInteger indexAI = new AtomicInteger(0);
        try {
            Files.list(path).parallel().filter(file -> {
                String filename = file.getFileName().toString().toLowerCase();
                return filename.endsWith("jpg") || filename.endsWith("jpeg");
            }).forEach(file -> {
                try {
                    String strOrigCreationDate = getOrigCreationDate(file.toFile());
                    if (strOrigCreationDate == null) {
                        strOrigCreationDate = ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm"));
                    }

                    if (indexAI.get() >= 9999) {
                        indexAI.set(0);
                    }

                    // 新文件名
                    String strDateFilename = strOrigCreationDate + "_IMG_" + Strings.padStart(String.valueOf(indexAI.incrementAndGet()), 4, '0');

                    String parent = file.getParent().toString();

                    // 重命名JPG
                    String newJpgFilename = strDateFilename + "." + getFileExtension(file.getFileName().toString()).toUpperCase();
                    Files.move(file, Paths.get(parent, newJpgFilename));

                    // 找对应的mov
                    String filename = file.getFileName().toString();
                    String movFilename = filename.substring(0, filename.lastIndexOf(".")) + ".MOV";
                    Path movPath = Paths.get(file.getParent().toString(), movFilename);
                    if (Files.exists(movPath)) {
                        String newMovFilename = strDateFilename + ".MOV";
                        Files.move(movPath, Paths.get(parent, newMovFilename));
                    }
                } catch (IOException e) {
                    logger.error("文件重命名失败 - " + e.getMessage(), e);
                }
            });
        } catch (IOException e) {
            logger.error("处理文件目录失败 - " + e.getMessage(), e);
        }
    }

    private String getOrigCreationDate(File file) {
        List<String> tags = Lists.newArrayList("Date/Time Original", "File Modified Date");
        try {
            Metadata metadata = JpegMetadataReader.readMetadata(file);
            String tagDesc = StreamSupport.stream(metadata.getDirectories().spliterator(), false)
                    .map(Directory::getTags)
                    .flatMap(Collection::stream)
                    .filter(tag -> tags.contains(tag.getTagName()))
                    .findFirst().orElseThrow(() -> new NullPointerException(String.format("File [%s] cannot find metadata named %s", file, String.join(",", tags))))
                    .getDescription();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(
                    "[EEE MMM dd HH:mm:ss XXX yyyy]" +
                    "[yyyy:MM:dd HH:mm:ss[a]]").withZone(ZoneId.systemDefault());
            if (Pattern.compile("[\u4e00-\u9fa5]").matcher(tagDesc).find()) {
                formatter.withLocale(Locale.CHINESE);
            } else {
                formatter.withLocale(Locale.ENGLISH);
            }

            return ZonedDateTime.parse(tagDesc, formatter).format(DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm"));
        } catch (JpegProcessingException | IOException | DateTimeParseException e) {
            logger.error("读取图片元数据失败 - " + e.getMessage(), e);
            return null;
        }
    }

    private String getFileExtension(String filename) {
        return filename.substring(filename.lastIndexOf(".") + 1);
    }

    class CacheInfo {
        private String screenName;
        private String itemId;

        public CacheInfo() {

        }

        public CacheInfo(String screenName, String itemId) {
            this.screenName = screenName;
            this.itemId = itemId;
        }

        public String getScreenName() {
            return screenName;
        }

        public void setScreenName(String screenName) {
            this.screenName = screenName;
        }

        public String getItemId() {
            return itemId;
        }

        public void setItemId(String itemId) {
            this.itemId = itemId;
        }
    }
}
